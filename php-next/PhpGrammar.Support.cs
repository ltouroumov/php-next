using System;
using System.Text;
using System.Globalization;
using Irony.Parsing;

namespace phpnext
{
	public partial class PhpGrammar
	{
		private NonTerminal PlusTerminal(string name, BnfTerm expr, BnfTerm sep = null)
		{
			var term = new NonTerminal(name);
			term.Rule = MakePlusRule(term, sep, expr);
			return term;
		}

		private NonTerminal StarTerminal(string name, BnfTerm expr, BnfTerm sep = null)
		{
			var term = new NonTerminal(name);
			term.Rule = MakeStarRule(term, sep, expr);
			return term;
		}

		private NonTerminal NonTerminal(string name, BnfExpression expr = null)
		{
			var term = new NonTerminal(name);
			if (expr != null) {
				term.Rule = expr;
			}
			return term;
		}

		public NonTerminal OptionalTerm(BnfTerm term)
		{
			var TermOpt = new NonTerminal(term.Name + "?");
			TermOpt.Rule = term | Empty;
			return TermOpt;
		}
	}

	public static class PhpGrammarExtensions
	{
		public static T As<T>(this T self, NonTerminal expr) where T : BnfTerm
		{
			if (expr.Rule == null) {
				expr.Rule = new BnfExpression(self);
			} else {
				expr.Rule |= self;
			}
			return self;
		}
	}
}

