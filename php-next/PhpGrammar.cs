using System;
using System.Linq;
using System.Text;
using System.Globalization;
using Irony.Parsing;

namespace phpnext
{
    [Language("php-next", "0.1", "What it says on the tin")]
	public partial class PhpGrammar : Grammar
	{
		public PhpGrammar()
		{
			#pragma warning disable 219

			//Declare comments
			var LineComment = new CommentTerminal("LineComment", "//", "\r", "\n");
			var BlockComment = new CommentTerminal("BlockComment", "/*", "*/");

			NonGrammarTerminals.Add(LineComment);
			NonGrammarTerminals.Add(BlockComment);

			//Declare symbols & keywords
			var Colon = ToTerm(":", "Colon");
			var DoubleColon = ToTerm("::", "DoubleColon");
			var SemiColon = ToTerm(";", "SemiColon");
			var OptColon = OptionalTerm(Colon);
			var OptSemiColon = OptionalTerm(SemiColon);

			var Dollar = ToTerm("$", "Dollar");
			var Arrow = ToTerm("->", "Arrow");
			var RevArrow = ToTerm("<-", "RevArrow");
			var FatArrow = ToTerm("=>", "FatArrow");
			var Splat = ToTerm("...", "Splat");
			var Comma = ToTerm(",", "Comma");
			var Equals = ToTerm("=", "equals");
			var OptComma = OptionalTerm(Comma);
			var NsSep = ToTerm(@"\", "NsSeparator");

			var Var = ToTerm("var");
			var Val = ToTerm("val");
			var Get = ToTerm("get");
			var Set = ToTerm("set");
			var Function = ToTerm("function");
			var Fun = ToTerm("fun");
			var Namespace = ToTerm("namespace");
			var Class = ToTerm("class");
			var Object = ToTerm("object");
			var Public = ToTerm("public");
			var Private = ToTerm("private");
			var Protected = ToTerm("protected");

			var If = ToTerm("if");
			var Unless = ToTerm("unless");
			var Then = ToTerm("then");
			var Else = ToTerm("else");
			var Match = ToTerm("match");
			var Case = ToTerm("case");
			var As = ToTerm("as");

			var For = ToTerm("for");
			var ForEach = ToTerm("foreach");
			var While = ToTerm("while");
			var Until = ToTerm("until");
			var Do = ToTerm("do");

			var Yield = ToTerm("yield");
			var Return = ToTerm("return");

			var True = ToTerm("true");
			var False = ToTerm("false");
			var Null = ToTerm("null");

			//Parenthesis pairs
			var Lpar = ToTerm("(", "LeftParenthesis");
			var Rpar = ToTerm(")", "RightParenthesis");
			RegisterBracePair("(", ")");

			//Brackets pairs
			var Lbr = ToTerm("{", "LeftBracket");
			var Rbr = ToTerm("}", "RightBracket");
			RegisterBracePair("{", "}");

			//Square brackets pairs
			var Lsbr = ToTerm("[", "LeftSquareBracket");
			var Rsbr = ToTerm("]", "RightSquareBracket");
			RegisterBracePair("[", "]");

			//Operators
			var BinaryOp = NonTerminal("BinaryOp", ToTerm("&&") | "||" |
			                           "+" | "-" | "*" | "/" | "%" |
			                           "|" | "&" | "^" | "<<" | ">>" |
			                           "==" | "<" | ">" | "<=" | ">=");

			var AssignOp = NonTerminal("AssignOp", Equals | "+=" | "-=" | "*=" | "/=" | "%=" | "&=" | "|=");

			//Literals
			var Number = new NumberLiteral("Number");
			var DoubleQuotedString = new StringLiteral("DoubleQuotedString", "\"", StringOptions.AllowsLineBreak | StringOptions.AllowsAllEscapes);
			var SingleQuotedString = new StringLiteral("SingleQuotedString", "'", StringOptions.AllowsLineBreak);
			var AnyString = NonTerminal("AnyString", DoubleQuotedString | SingleQuotedString);

			//Identifiers
			var AnyIdentifier = IdentifierTerm();
			var VarIdentifier = NonTerminal("VarIdentifier",  Dollar + AnyIdentifier);
			var FieldIdentifier = NonTerminal("FieldIdentifier", AnyIdentifier);
			var FuncIdentifier = NonTerminal("FuncIdentifier", AnyIdentifier);
			var TypeIdentifier = NonTerminal("TypeIdentifier", AnyIdentifier);
			var OptTypeIdentifier = OptionalTerm(TypeIdentifier);
			var NsPart = NonTerminal("NsPartIdentifier", NsSep + AnyIdentifier);
			var NsIdentifier = PlusTerminal("NsIdentifier", NsPart);

			//Expressions
			var ConstExpression = NonTerminal("ConstExpression", True | False | Null | Number | AnyString);
			var DynamicExpression = NonTerminal("Expression", VarIdentifier);
			var Expression = NonTerminal("Expression", ConstExpression | DynamicExpression);
			var OptExpression = OptionalTerm(Expression);
			var EnclosedExpression = NonTerminal("EnclosedExpression", Lpar + Expression + Rpar)
				.As(DynamicExpression);
			var BinaryExpression = NonTerminal("BinaryExpression", Expression + BinaryOp + Expression)
				.As(DynamicExpression);

			//Statements
			var SingleStatement = NonTerminal("SingleStatement", Expression);
			var OptSingleStatement = OptionalTerm(SingleStatement);
			var FullSingleStatement = NonTerminal("FullSingleStatement", SingleStatement + SemiColon);
			var BlockStatement = NonTerminal("BlockSatement");
			var Statement = NonTerminal("Statement", FullSingleStatement | BlockStatement);
			var StatementList = PlusTerminal("StatementList", Statement);

			//Variable Declaration and Assigniation
			var ConstAssign = NonTerminal("ConstAssign", Equals + ConstExpression);
			var OptConstAssign = OptionalTerm(ConstAssign);
			var SimpleAssign = NonTerminal("SimpleAssign", Equals + Expression);
			var ComplexAssign = NonTerminal("Assigniation", AssignOp + Expression);
			var Assign = NonTerminal("Assign", SimpleAssign | ComplexAssign);
			var OptAssign = OptionalTerm(Assign);

			var SimpleVarDeclaration = NonTerminal("SimpleVarDeclaration", Var + VarIdentifier);
			var VarDeclaration = NonTerminal("VarDeclaration", SimpleVarDeclaration + OptAssign)
				.As(SingleStatement);

			var SimpleValDeclaration = NonTerminal("SimpleValDeclaration", Val + VarIdentifier);
			var ValDeclaration = NonTerminal("ValDeclaration", SimpleValDeclaration + SimpleAssign)
				.As(SingleStatement);

			var VarAssigniation = NonTerminal("VarAssigniation", VarIdentifier + Assign)
				.As(Expression);

			var OptUnapplySplat = OptionalTerm(NonTerminal("UnapplySplat", DoubleColon + VarIdentifier));
			var UnapplyVarList = PlusTerminal("UnapplyVarList", VarIdentifier, Comma);
			var ArrayUnapply = NonTerminal("UnapplyBody", Lsbr + UnapplyVarList + OptUnapplySplat + Rsbr);
			var UnapplyValArray = NonTerminal("UnapplyValArray", Val + ArrayUnapply + SimpleAssign)
				.As(SingleStatement);
			var UnapplyVarArray = NonTerminal("UnapplyVarArray", Var + ArrayUnapply + SimpleAssign)
				.As(SingleStatement);

			//Array
			var ArrayKey = NonTerminal("ArrayKey", ConstExpression + FatArrow);
			var KeyValueArrayItem = NonTerminal("KeyValueArrayItem", ArrayKey + Expression);
			var ArrayItem = NonTerminal("ArrayItem", KeyValueArrayItem | Expression);
			var ArrayBody = StarTerminal("ArrayBody", ArrayItem, Comma);
			var Array = NonTerminal("Array", Lsbr + ArrayBody + Rsbr)
				.As(Expression);

			//Blocks
			var Block = NonTerminal("Block", Lbr + StatementList + Rbr);
			var OptBlock = OptionalTerm(Block);
			var ReturnStatement = NonTerminal("ReturnStatement", Return + Expression)
				.As(SingleStatement);

			//Method Definition and Invocation
			var FuncBody = NonTerminal("FuncBody", Block | FullSingleStatement);
			var FuncArgumentDef = NonTerminal("FuncArgumentDef", OptTypeIdentifier + VarIdentifier + OptConstAssign);
			var SplatArgumentDef = NonTerminal("SplatArgumentDef", DoubleColon + VarIdentifier);
			var OptSplatArgumentDef = OptionalTerm(SplatArgumentDef);
			var FuncArgumentListDef = StarTerminal("FuncArgumentListDef", FuncArgumentDef, Comma);
			var FuncArgumentsDef = NonTerminal("FuncArgumentsDef", FuncArgumentListDef + OptSplatArgumentDef);
			var EnclosedFuncArgumentsDef = NonTerminal("EnclosedFuncArgumentsDef", Lpar + FuncArgumentsDef + Rpar);
			var FuncDefinition = NonTerminal("FuncDefinition", Function + FuncIdentifier + EnclosedFuncArgumentsDef + Equals + FuncBody)
				.As(BlockStatement);

			var FuncArgumentsList = StarTerminal("FuncArgumentsList", Expression, Comma);
			var FuncArguments = NonTerminal("FuncArguments", Lpar + FuncArgumentsList + Rpar);
			var FuncCall = NonTerminal("FuncCall", FuncIdentifier + FuncArguments)
				.As(DynamicExpression);


			//Object accessors
			var ObjectGetter = NonTerminal("ObjectAccessor", Expression + Arrow + FieldIdentifier)
				.As(DynamicExpression);
			var ObjectSetter = NonTerminal("ObjectSetter", ObjectGetter + AssignOp + Expression)
				.As(DynamicExpression);
			var MethodCall = NonTerminal("MethodCall", Expression + Arrow + FieldIdentifier + FuncArguments)
				.As(DynamicExpression);

			var AnyVar = NonTerminal("AnyVar", VarIdentifier | ObjectGetter);
			var PreUnaryOp = NonTerminal("PreUnaryOp", ToTerm("-") | "!" | "~");
			var PreUnaryOperation = NonTerminal("PreUnaryOperation", PreUnaryOp + AnyVar)
				.As(Expression);

			//Conditionals
			var ConditionBody = NonTerminal("ConditionBody", Block | Expression);
			var IfHeader = NonTerminal("IfHeader", If + EnclosedExpression);
			var UnlessHeader = NonTerminal("UnlessHeader", Unless + EnclosedExpression);
			var ConditionHeader = NonTerminal("ConditionHeader", IfHeader | UnlessHeader);
			var OptConditionHeader = OptionalTerm(ConditionHeader);

			var ThenBlock = NonTerminal("ThenBlock", ConditionBody);
			var ElseBlock = NonTerminal("ElseBlock", Else + ConditionBody);

			var ConditionBlock = NonTerminal("ConditionBlock", ConditionHeader + ThenBlock)
				.As(BlockStatement);
			var ConditionExpression = NonTerminal("ConditionExpression", ConditionHeader + ThenBlock + ElseBlock)
				.As(DynamicExpression).As(BlockStatement);
			var ConditionStatement = NonTerminal("ConditionStatement", SingleStatement + ConditionHeader);

			//Loops
			var WhileLoop = NonTerminal("WhileLoop", While + EnclosedExpression + Block)
				.As(BlockStatement);
			var UntilLoop = NonTerminal("UntilLoop", Until + EnclosedExpression + Block)
				.As(BlockStatement);

			var WhilePostLoop = NonTerminal("WhilePostLoop", Do + Block + While + EnclosedExpression)
				.As(BlockStatement);
			var UntilPostLoop = NonTerminal("UntilPostLoop", Do + Block + Until + EnclosedExpression)
				.As(BlockStatement);

			//For loop
			var ForHeader = NonTerminal("BasicForHeader", Lpar + OptSingleStatement + SemiColon + OptExpression + SemiColon + OptSingleStatement + Rpar);
			var ForLoop = NonTerminal("BasicForLoop", For + ForHeader + Block)
				.As(BlockStatement);

			//For generator
			var ForExprGenerator = NonTerminal("ForExprGenerator", VarIdentifier + RevArrow + Expression + OptConditionHeader);
			var ForExprGeneratorList = PlusTerminal("ForExprGeneratorList", ForExprGenerator, Comma);
			var ForExprHeader = NonTerminal("ForExprHeader", Lpar + ForExprGeneratorList + Rpar);
			var ForExprBody = NonTerminal("ForExprBody", Block | Expression);
			var ForExpr = NonTerminal("ForExpr", For + ForExprHeader + Yield + ForExprBody)
				.As(DynamicExpression);
			var ForStatement = NonTerminal("ForStatement", For + ForExprHeader + ForExprBody)
				.As(BlockStatement);

			//Match Expression
			var ClassUnapply = NonTerminal("ClassUnapply", TypeIdentifier + OptionalTerm(EnclosedFuncArgumentsDef));
			var TypeUnapply = NonTerminal("TypeUnapply", VarIdentifier + As + TypeIdentifier);

			var MatchItemOptions = NonTerminal("MatchItemHeaderOptions", ArrayUnapply | ClassUnapply | TypeUnapply);
			var MatchItemHeader = NonTerminal("MatchItemHeader", MatchItemOptions + OptConditionHeader);
			var MatchItemBody = NonTerminal("MatchItemBody", Block | Expression);
			var MatchItem = NonTerminal("MatchItem", Case + MatchItemHeader + FatArrow + MatchItemBody);
			var MatchItemList = PlusTerminal("MatchItemList", MatchItem);
			var MatchBody = NonTerminal("MatchBody", Lbr + MatchItemList + Rbr);
			var MatchExpr = NonTerminal("MatchExpr", Match + EnclosedExpression + MatchBody)
				.As(Expression)
				.As(BlockStatement);

			//Anonymous Method
			var CurryVar = NonTerminal("CurryVar", ToTerm("$1") | "$2" | "$3" | "$4" | "$5")
				.As(Expression);

			//var CurryExpr = NonTerminal("CurryExpr", CustomActionHere(this.ResolveAnonFuncConflict) + Expression);
			var CurryAnonFunc = NonTerminal("TinyAnonFunc", Lbr + Lbr + Expression + Rbr + Rbr);

			var SmallAnonFuncHead = NonTerminal("AnonFuncHead", FuncArgumentsDef + FatArrow);
			var SmallAnonFuncBody = NonTerminal("SmallAnonFuncBody", SingleStatement | Expression);
			var SmallAnonFunc = NonTerminal("SmallAnonFunc", Lbr + SmallAnonFuncHead + SmallAnonFuncBody + Rbr);

			var BigAnonFunbBody = NonTerminal("BigAnonFuncBody", StatementList | SingleStatement);
			var BigAnonFunc = NonTerminal("BigAnonFunc", Lbr + EnclosedFuncArgumentsDef + FatArrow + BigAnonFunbBody + Rbr);

			var PartialAnonFunc = NonTerminal("PartialAnonFunc", Lbr + MatchItemList + Rbr);

			var AnonFunc = NonTerminal("AnonFunc", CurryAnonFunc | SmallAnonFunc | BigAnonFunc | PartialAnonFunc)
				.As(DynamicExpression);

			//Namespace
			var NamespaceStatement = NonTerminal("NamespaceStatement", Namespace + NsIdentifier)
				.As(SingleStatement);
			var NamespaceBlock = NonTerminal("NamespaceBlock", Namespace + NsIdentifier + Block)
				.As(BlockStatement);

			//Class Definition
			//-- Annotations
			var AnnotationArgValue = NonTerminal("AnnotationArgValue", ConstExpression);
			var AnnotationArgKey = NonTerminal("AnnotationArgKey", FieldIdentifier + FatArrow + AnnotationArgValue);
			var AnnotationArg = NonTerminal("KeyAnnotationArgList", AnnotationArgKey | AnnotationArgValue);
			var AnnotationArgs = StarTerminal("AnnotationArgs", AnnotationArg, Comma);
			var Annotation = NonTerminal("Annotation", TypeIdentifier + Lpar + AnnotationArgs + Rpar)
				.As(AnnotationArgValue);
			var AnnotationList = PlusTerminal("AnnotationList", Annotation, Comma);
			var Annotations = NonTerminal("Annotations", Lsbr + AnnotationList + Rsbr);
			var OptAnnotations = OptionalTerm(Annotations);

			//-- Generic
			var AccessModifier = NonTerminal("AccessModifier", Public | Protected | Private);
			var OptAccessModifier = OptionalTerm(AccessModifier);

			//-- Methods
			var MethodDefinition = NonTerminal("MethodDefinition", OptAnnotations + OptAccessModifier + FuncDefinition);

			//-- Properties
			var OptGetterBlock = OptionalTerm(Block);
			var SetterBlock = NonTerminal("SetterBlock", Lpar + OptTypeIdentifier + VarIdentifier + Rpar + Block);
			var OptSetterBlock = OptionalTerm(SetterBlock);

			var PropertyGet = NonTerminal("PropertyGet", OptAccessModifier + Get + OptGetterBlock + SemiColon);
			var OptPropertyGet = OptionalTerm(PropertyGet);
			var PropertySet = NonTerminal("PropertySet", OptAccessModifier + Set + OptSetterBlock + SemiColon);
			var OptPropertySet = OptionalTerm(PropertySet);

			var PropertyBody = NonTerminal("PropertyBody", Lbr + OptPropertyGet + OptPropertySet + Rbr);
			var PropertyDefinition = NonTerminal("PropertyDefinition", OptAnnotations + AccessModifier + VarIdentifier + PropertyBody);

			//-- Fields
			var VarFieldDefinition = NonTerminal("VarFieldDefinition", OptAnnotations + OptAccessModifier + Var + VarIdentifier + OptAssign + SemiColon);
			var ValFieldDefinition = NonTerminal("ValFieldDefinition", OptAnnotations + OptAccessModifier + Val + VarIdentifier + Assign + SemiColon);
			var FieldDefinition = NonTerminal("FieldDefinition", VarFieldDefinition | ValFieldDefinition);

			var ClassItem = NonTerminal("ClassItem", FieldDefinition | PropertyDefinition | MethodDefinition);
			var ClassItemList = StarTerminal("ClassItemList", ClassItem);
			var ClassBody = NonTerminal("ClassBody", Lbr + ClassItemList + Rbr);

			var ClassHeader = NonTerminal("ClassHeader", Class + TypeIdentifier);
			var ClassDefinition = NonTerminal("ClassDefinition", ClassHeader + ClassBody)
				.As(BlockStatement);

			var ObjectHeader = NonTerminal("ObjectHeader", Object + TypeIdentifier);
			var ObjectDefinition = NonTerminal("ObjectDefinition", ObjectHeader + ClassBody)
				.As(BlockStatement);

			this.Root = StatementList;
		}

		private static string[] CurryVars = { "$1", "$2", "$3", "$4", "$5" };
		private void ResolveAnonFuncConflict(ParsingContext context, CustomParserAction customAction)
		{
			var scanner = context.Parser.Scanner;

			var foundCurry = false;
			var baseToken = context.CurrentToken.Terminal.Name;
			var token = context.CurrentToken;
			scanner.BeginPreview();


			while(true) {
				if (CurryVars.Contains(token.Terminal.Name)) {
					foundCurry = true;
					break;
				}
				if (token.Terminal.Name == "RightBracket") {
					break;
				}

				token = scanner.GetToken();
			}

			scanner.EndPreview(true);

			var action = default(ParserAction);
			if (foundCurry) {
				action = customAction.ShiftActions.First(a => a.Term.Name == "Expression");
			} else {
				action = customAction.ShiftActions.First(a => a.Term.Name == baseToken);
			}
			action.Execute(context);
		}

		private IdentifierTerminal IdentifierTerm()
		{
			var id = new IdentifierTerminal("Identifier");

			id.StartCharCategories.AddRange(new UnicodeCategory[] {
				UnicodeCategory.UppercaseLetter, //Ul
				UnicodeCategory.LowercaseLetter, //Ll
				UnicodeCategory.TitlecaseLetter, //Lt
				UnicodeCategory.ModifierLetter,  //Lm
				UnicodeCategory.OtherLetter,     //Lo
				UnicodeCategory.LetterNumber     //Nl
			});

			id.CharCategories.AddRange(id.StartCharCategories); //letter-character categories
			id.CharCategories.AddRange(new UnicodeCategory[] {
				UnicodeCategory.DecimalDigitNumber,   //Nd
				UnicodeCategory.ConnectorPunctuation, //Pc
				UnicodeCategory.SpacingCombiningMark, //Mc
				UnicodeCategory.NonSpacingMark,       //Mn
			});

			id.CharsToRemoveCategories.Add(UnicodeCategory.Format);

			return id;
		}
	}
}

