﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Parsing;
using phpnext;

namespace ConsoleRuntime
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var source = GetSourceCode();

            var grammar = new PhpGrammar();
            var langData = new LanguageData(grammar);
            var parser = new Parser(langData);
            var tree = parser.Parse(source);
            if (tree.HasErrors())
            {
                Console.WriteLine("Parser Messages:");
                foreach (var message in tree.ParserMessages)
                {
                    Console.WriteLine("{0} near '{3}' at L{1}:C{2}",
                                      message.Message,
                                      message.Location.Line + 1,
                                      message.Location.Column + 1,
                                      CodeNear(source, message.Location.Position),
                                      message.ParserState
                                      );
                }
            }
            else
            {
                VisitNodes(tree.Root);
            }
        }

        public static string GetSourceCode()
        {
            return File.ReadAllText("./test.php");
        }

        public static string CodeNear(string source, int position, int context = 20)
        {
            var before = Math.Max(0, position - context);
            var size = context * 2;
            if (before + size > source.Length)
            {
                size = source.Length - before;
            }

            var snippet = source.Substring(before, size).Insert(position - before, "~@~");
            return snippet.Replace("\r", @"\r").Replace("\n", @"\n").Replace("\t", @"\t");
        }

        //Structural nodes (can be skipped)
        private static string[] SkipNodes = {
			"SemiColon", "Dollar", "Comma", "Colon", "DoubleColon",
			"Splat", "NsSeparator", "FatArrow",
			"LeftParenthesis", "RightParenthesis",
			"LeftBracket", "RightBracket",
			"LeftSquareBracket", "RightSquareBracket",
			"equals", "val", "var", "function", "fun", "namespace", "class", "object" };

        //Can be hidden (but no skipped)
        private static string[] HiddenNodes = {
			"AnyString",
			"OptTypeIdentifier",
			"OptConstAssign",
			"OptExpression",
			"OptSplatArgumentDef",
			"OptUnapplySplat",
			"OptAccessModifier",
			"OptAnnotations",
			"ClassItemList", "ClassBody",
			"BigAnonFunc", "SmallAnonFunc",
			"FullSingleStatement",
			"FuncArgumentsList"
		};

        private static int depth = 0;
        private static void VisitNodes(ParseTreeNode current)
        {
            var isVisible = !IsHidden(current.Term);
            if (isVisible)
            {
                depth++;
                Console.WriteLine("{0}{1} (", Indent(), current.Term);
            }
            foreach (var node in current.ChildNodes)
            {
                if (node.Term is Terminal && CanSkip(node.Term))
                    continue;

                if (node.Term is NonTerminal)
                {
                    VisitNodes(node);
                }
                else
                {
                    Console.WriteLine("{0}{1} = {2}", Indent(1), node.Term, node.Token.Text);
                }
            }
            if (isVisible)
            {
                Console.WriteLine("{0}) {1}", Indent(), current.Term);
                depth--;
            }
        }

        private static bool IsHidden(BnfTerm term)
        {
            return HiddenNodes.Contains(term.Name);
        }

        private static bool CanSkip(BnfTerm term)
        {
            return SkipNodes.Contains(term.Name);
        }

        private static string Indent(int plus = 0)
        {
            return String.Join(" ", Enumerable.Range(0, depth + plus).Select(x => String.Empty));
        }
    }
}
