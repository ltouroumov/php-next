/*function toto(string $name = "World") = {
	println(if ($name == "toto") "Hello Toto, welcome back !" else "Hello" + ($name * 2));
}
function main($args) = toto("World");

if (true) {
	println("It's True");
} else {
	println("It's False");
}

unless (false) {
	println("It's False");
} else {
	println("It's True");
}

while ($true) {
	println($data);
}

for (var $i = 0;$i < 10;$i += 1) {
	println("Hello World");
}

val $goodPricedShoes = for (
		$item <- $items if($item->category->contains("Shoes")),
		$price <- $item->prices if ($price < 10)
	) yield $price;

val $toto = ["Hello", "World"];

namespace \Toto\Tata;

namespace \Hola {
    function test($args1 :: $args) = println($args);
}

var [$toto, $tata :: $data] = test();

class Test {
    public $toto { get { return $this->_toto; }; set($value){ $this->_toto = $value; }; }
    
    [Toto(1, 2, Test(), Key => Hello())]
    public function test() = println("Test");
}
object Test { }

match ($test) {
	case [$x, $y, $z]       => $xs
	case Test($toto, $tata) => $toto + $tata
	case $b as Test         => $b->get()
} */

println(
	{{ $1->GetFoo() }},
	{ $x => $x * 2 },
	{ ($x) => println("Hello"); return $x * 2; },
	{ case Some($x) => $x * 2 case None => 0 }
);